import { User } from "./entity/User"
import { Role } from "./entity/Role"
import { Type } from "./entity/Type"
import { Product } from "./entity/Product"
import { Order } from "./entity/Order"
import { OrderItem } from "./entity/OrderItem"
import { AppDataSource } from "./data-source"

AppDataSource.initialize().then(async () => {
    await AppDataSource.manager.clear(OrderItem)
    await AppDataSource.manager.clear(Order)
    await AppDataSource.manager.clear(User)
    await AppDataSource.manager.clear(Product)
    await AppDataSource.manager.clear(Role)
    await AppDataSource.manager.clear(Type)
}).catch(error => console.log(error))
