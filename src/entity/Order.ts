import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { OrderItem } from "./OrderItem";
import { User } from "./User";

@Entity()
export class Order{
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    qty: number

    @Column()
    total: number

    @CreateDateColumn()
    created: Date

    @UpdateDateColumn()
    upDated: Date

    @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
    orderItems: OrderItem[]

    @ManyToOne(() => User, (user) => user.orders)
    user: User;
    
}