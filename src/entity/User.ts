import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToMany, JoinTable, OneToMany } from "typeorm"
import { Role } from "./Role"
import { Order } from "./Order"

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    email: string

    @Column()
    password: string

    @Column()
    fullName: string

    @Column()
    gender: string

    @CreateDateColumn()
    createDate: Date

    @UpdateDateColumn()
    updateDate: Date

    @ManyToMany(() => Role, (role) => role.users)
    @JoinTable()
    roles: Role[]

    @OneToMany(() => Order, (order) => order.user)
    orders: Order[]
}
