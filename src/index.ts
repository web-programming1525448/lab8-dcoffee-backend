import { AppDataSource } from "./data-source"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {
    console.log("Inserting a new user into the database...")
}).catch(error => console.log(error))
