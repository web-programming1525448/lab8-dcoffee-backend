import { AppDataSource } from "./data-source";
import { Order } from "./entity/Order";
import { OrderItem } from "./entity/OrderItem";
import { Product } from "./entity/Product";
import { User } from "./entity/User";

const orderDto = {
  orderItems: [
    { productId: 1, qty: 1 },
    { productId: 2, qty: 2 },
    { productId: 4, qty: 1 },
  ],
  userId: 2,
};

AppDataSource.initialize()
  .then(async () => {
    const orderRepository = AppDataSource.getRepository(Order);
    const userRepository = AppDataSource.getRepository(User);
    const OrderItemRepository = AppDataSource.getRepository(OrderItem);
    const productRepository = AppDataSource.getRepository(Product);

    const user = await userRepository.findOneBy({id: orderDto.userId})

    const order = new Order()
    order.user = user
    order.total =0
    order.qty = 0
    order.orderItems = []
    for(const oi of orderDto.orderItems){
        const orderItem = new OrderItem()
        orderItem.product = await productRepository.findOneBy({id: oi.productId})
        orderItem.name = orderItem.product.name
        orderItem.price = orderItem.product.price
        orderItem.qty = oi.qty
        orderItem.total = orderItem.price * orderItem.qty
        await OrderItemRepository.save(orderItem)
        order.orderItems.push(orderItem)
        order.qty += orderItem.qty
        order.total += orderItem.total
    }
    await orderRepository.save(order)
    
    const orders = await orderRepository.find({relations: {user: true, orderItems: true}})
    console.log(JSON.stringify(orders, null, 2))
  })
  .catch((error) => console.log(error));
