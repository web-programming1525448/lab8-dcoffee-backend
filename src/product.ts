import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"
import { Type } from "./entity/Type"

AppDataSource.initialize().then(async () => {
    const typeRepository = AppDataSource.getRepository(Type)
    const productRepository = AppDataSource.getRepository(Product)
    const drink = await typeRepository.findOneBy({id: 1})
    const bakery = await typeRepository.findOneBy({id: 2})
    const food = await typeRepository.findOneBy({id: 3})
    await productRepository.clear()
    var product = new Product()
    product.id = 1
    product.name = "Coffee"
    product.price = 50
    product.type = drink
    await productRepository.save(product)

    product = new Product()
    product.id = 2
    product.name = "Green Tea"
    product.price = 40
    product.type = drink
    await productRepository.save(product)

    product = new Product()
    product.id = 3
    product.name = "Cocoa"
    product.price = 60
    product.type = drink
    await productRepository.save(product)

    product = new Product()
    product.id = 4
    product.name = "Cake 1"
    product.price = 70
    product.type = bakery
    await productRepository.save(product)

    product = new Product()
    product.id = 5
    product.name = "Rice"
    product.price = 10
    product.type = food
    await productRepository.save(product)

    const products = await productRepository.find({relations: {type: true}})
    console.log(products)
}).catch(error => console.log(error))
