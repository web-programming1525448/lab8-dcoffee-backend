import { AppDataSource } from "./data-source"
import { Role } from "./entity/Role"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {
    const userRepository = AppDataSource.getRepository(User)
    const roleRepository = AppDataSource.getRepository(Role)
    const adminRole = await roleRepository.findOneBy({id: 1})
    const userRole = await roleRepository.findOneBy({id: 2})
    await userRepository.clear()
    console.log("Inserting a new user into the database...")
    var user = new User()
    user.id = 1
    user.email = "user1@gmail.com"
    user.password = "pass1234"
    user.fullName = "user1"
    user.gender = "male"
    user.roles = []
    user.roles.push(adminRole)
    user.roles.push(userRole)
    console.log("Saved a new user with id: " + user.id)
    await userRepository.save(user)

    console.log("Inserting a new user into the database...")
    user = new User()
    user.id = 2
    user.email = "user2@gmail.com"
    user.password = "pass1234"
    user.fullName = "user2"
    user.gender = "male"
    user.roles = []
    user.roles.push(userRole)
    console.log("Saved a new user with id: " + user.id)
    await userRepository.save(user)

    console.log("Inserting a new user into the database...")
    user = new User()
    user.id = 3
    user.email = "user3@gmail.com"
    user.password = "pass1234"
    user.fullName = "user3"
    user.gender = "male"
    user.roles = []
    user.roles.push(userRole)
    console.log("Saved a new user with id: " + user.id)
    await userRepository.save(user)

    console.log("Loading users from the database...")
    const users = await userRepository.find({relations: {roles: true}})
    console.log(JSON.stringify(users,null,2))

    const roles =await roleRepository.find({relations: {users: true}})
    console.log(JSON.stringify(roles,null,2))

}).catch(error => console.log(error))
